import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StatusBar, KeyboardAvoidingView, View, Text, TextInput } from 'react-native';
import { FormLabel, FormInput, Button } from 'react-native-elements';
import { connect } from 'react-redux';

import { Container } from '../components/Container';
//import { Logo } from '../components/Logo';
//import { InputWithButton } from '../components/TextInput';
//import { ClearButton } from '../components/Button';
//import { LastConverted } from '../components/Text';
//import { Header } from '../components/Header';
//import { connectAlert } from '../components/Alert';

//import { changeCurrencyAmount, swapCurrency, getInitialConversion } from '../actions/currencies';

const ROOT_URL = 'api/create';

class Login extends Component {
    static propTypes = {
        navigation: PropTypes.object,
        dispatch: PropTypes.func,
    };

    constructor(props) {
        super();
        this.state = {
            phone: '',
            email: '',
            password: '',
            username: ''
        }
    }

    componentWillMount() {

    }

    componentWillReceiveProps(nextProps) {

    }


    handleSubmit = async () => {
        this.props.navigation.navigate('Core');
        try {
            let { data } = await axios.post(`${ROOT_URL}`, {
                phone: this.state.phone, name: this.state.name,
            });

        } catch (err) {
            console.log(err);
        }
    };

    render() {


        return (
            <Container>
                <Text>Login</Text>
                <View style={{ marginBottom: 10 }}>
                    <FormLabel>Enter Email</FormLabel>
                    <FormInput
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                    />
                </View>
                <View style={{ marginBottom: 10 }}>
                    <FormLabel>Enter Password</FormLabel>
                    <FormInput
                        secureTextEntry
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })}
                    />
                </View>
                <Button onPress={this.handleSubmit} title="Submit"/>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    const base = {};

    return {
        base,
    };
};

export default connect(mapStateToProps)(Login);
