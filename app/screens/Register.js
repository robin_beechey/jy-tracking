import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StatusBar, KeyboardAvoidingView, View, Text, TextInput, Alert } from 'react-native';
import { FormLabel, FormInput, Button } from 'react-native-elements';
import { connect } from 'react-redux';

import { Container } from '../components/Container';

import { registerUser } from '../actions/user';

const ROOT_URL = 'jytracker/register_account.php';

class Register extends Component {
    static propTypes = {
        navigation: PropTypes.object,
        dispatch: PropTypes.func,
    };

    constructor(props) {
        super();
        this.state = {
            phone: '',
            email: '',
            password: '',
            username: ''
        }
    }

    componentWillMount() {

    }

    componentWillReceiveProps(nextProps) {

    }


    handleSubmit = async () => {

        //try {
        //    let { data } = await axios.post(`${ROOT_URL}`, {
        //        phone: this.state.phone, name: this.state.name,
        //    });
        //    //this.props.navigation.navigate('Core');
        //    //this.props.dispatch(registerUser(data));
        //
        //} catch (err) {
        //    console.log(err);
        //}

        let {name} = this.state;
        let {email} = this.state;
        let {password} = this.state;
        let {phone} = this.state;

        //fetch(`${ROOT_URL}`, {
        //    method: 'POST',
        //    headers: {
        //        'Accept': 'application/json',
        //        'Content-Type': 'application/json',
        //    },
        //    body: JSON.stringify({
        //        name: name,
        //        email: email,
        //        password: password,
        //        phone: phone
        //    })
        //}).then((response) => response.json())
        //    .then((responseJson) => {
        //        // Showing response message coming from server after inserting records.
        //        //this.props.dispatch(registerUser(responseJson));
        //        //this.props.navigation.navigate('Core');
        //        Alert.alert(responseJson);
        //    }).catch((error) => {
        //    console.error(error);
        //});

        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify({
                title: 'foo',
                body: 'bar',
                userId: 1
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then(json => {
                this.props.dispatch(registerUser(json));
                //this.props.navigation.navigate('Core');
                //Alert.alert(json);
                console.log(json)
            })



    };

    render() {


        return (
            <Container>
                <Text>Register</Text>
                <View style={{ marginBottom: 10 }}>
                    <FormLabel>Enter Username</FormLabel>
                    <FormInput
                        value={this.state.username}
                        onChangeText={username => this.setState({ username })}
                    />
                </View>

                <View style={{ marginBottom: 10 }}>
                    <FormLabel>Enter Email</FormLabel>
                    <FormInput
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                    />
                </View>
                <View style={{ marginBottom: 10 }}>
                    <FormLabel>Enter Phone</FormLabel>
                    <FormInput
                        value={this.state.phone}
                        onChangeText={phone => this.setState({ phone })}
                    />
                </View>
                <View style={{ marginBottom: 10 }}>
                    <FormLabel>Enter Password</FormLabel>
                    <FormInput
                        secureTextEntry
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })}
                    />
                </View>
                <Button onPress={this.handleSubmit} title="Submit"/>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    const base = {};

    return {
        base,
    };
};

export default connect(mapStateToProps)(Register);
