import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StatusBar, KeyboardAvoidingView, View, Text, TextInput } from 'react-native';
import { FormLabel, FormInput, Button } from 'react-native-elements';
import { connect } from 'react-redux';

import { Container } from '../components/Container';
//import { Logo } from '../components/Logo';
//import { InputWithButton } from '../components/TextInput';
//import { ClearButton } from '../components/Button';
//import { LastConverted } from '../components/Text';
//import { Header } from '../components/Header';
//import { connectAlert } from '../components/Alert';

//import { changeCurrencyAmount, swapCurrency, getInitialConversion } from '../actions/currencies';

const ROOT_URL = 'api/create';

class Location extends Component {
    static propTypes = {
        navigation: PropTypes.object,
        dispatch: PropTypes.func,
    };

    constructor(props) {
        super();
        this.state = {
            phone: '',
            email: '',
            password: '',
            username: ''
        }
    }

    componentWillMount() {

    }

    componentWillReceiveProps(nextProps) {

    }


    //handleOptionsPress = () => {
    //    this.props.navigation.navigate('Options');
    //};

    handleSubmit = async () => {
        this.props.navigation.navigate('Group');
        try {
            let { data } = await axios.post(`${ROOT_URL}`, {
                phone: this.state.phone, name: this.state.name,
            });


        } catch (err) {
            console.log(err);
        }
    };

    render() {


        return (
            <Container>
                <View style={{ marginBottom: 10 }}>
                    <Text>This is a location page</Text>
                </View>
                <Button onPress={this.handleSubmit} title="Submit"/>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    const base = {};

    return {
        base,
    };
};

export default connect(mapStateToProps)(Location);
