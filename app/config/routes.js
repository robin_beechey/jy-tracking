import { StatusBar } from 'react-native';
import { StackNavigator, TabNavigator } from 'react-navigation';

import Login from '../screens/Login';
import Register from '../screens/Register';
import Core from '../screens/Core';
import Group from '../screens/Group';
import Location from '../screens/Location';
import Notes from '../screens/Notes';
import Session from '../screens/Session';
import Activities from '../screens/Activities';

const AuthStack = TabNavigator(
    {
        Register: {
            screen: Register,
            navigationOptions: {
                headerTitle: 'Register',
            }
        },
        Login: {
            screen: Login,
            navigationOptions: {
                headerTitle: 'Login',
            }
        }
    }
);


const HomeStack = StackNavigator(
    {
        Core: {
            screen: Core,
            navigationOptions: {
                headerTitle: 'Core',
            },
        },
        Location: {
            screen: Location,
            navigationOptions: {
                headerTitle: 'Location',
            },
        },
        Group: {
            screen: Group,
            navigationOptions: {
                headerTitle: 'Group',
            },
        },
        Activities: {
            screen: Activities,
            navigationOptions: {
                headerTitle: 'Activities',
            },
        },
        Session: {
            screen: Session,
            navigationOptions: {
                headerTitle: 'Session',
            },
        },
    },

    {
        headerMode: 'screen',
    }
);


export default StackNavigator(
    {
        Auth: {
            screen: AuthStack,
        },
        Core:{
            screen: HomeStack
        }
    },
    {
        mode: 'screen',
        headerMode: 'none',
        cardStyle: {paddingTop: StatusBar.currentHeight},
    }
);
