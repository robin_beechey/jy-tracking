import { REGISTER_USER } from '../actions/user';
import { LOGIN_USER } from '../actions/user';
import { LOGOUT_USER } from '../actions/user';

const initialState = {
  user: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_USER:
      return { ...state, user: action.user };
    case LOGIN_USER:
      return { ...state, user: action.user };
    default:
      return state;
  }
};
