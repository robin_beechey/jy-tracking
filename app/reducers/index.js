import { combineReducers } from 'redux';

//import currencies from './currencies';
import user from './user';

export default combineReducers({
  user,
});
