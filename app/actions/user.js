export const REGISTER_USER = 'REGISTER_USER';
export const LOGIN_USER = 'LOGIN_USER';
export const LOGOUT_USER = 'LOGOUT_USER';

export const registerUser = user => ({
  type: REGISTER_USER,
  user,
});

export const loginUser = user => ({
  type: LOGIN_USER,
  user,
});

export const logoutUser = user => ({
  type: LOGOUT_USER,
  user,
});
